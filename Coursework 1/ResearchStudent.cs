﻿//Calum McCall
//This class contains the ResearchStudent class, its paramaters and verification. This class inherits from the Student class
//Last modified 22/10/2015

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coursework_1
{
    //The ResearchStudent inherits from student adding two additional properties
    class ResearchStudent : Student
    {
        private string topic; //The topic they are researching
        private string supervisor; //Their supervisor

        public string Topic
        //Initialising and allowing access to matriculation
        {
            get
            {
                return topic;
            }
            set
            {
                //Makes sure that the field isn't empty
                if (value.Length == 0)
                {
                    throw new ArgumentException("No topic entered.");
                }
                topic = value;
            }
        }

        public String Supervisor
        //Initialising and allowing access to matriculation
        {
            get
            {
                return supervisor;
            }
            set
            {
                //Makes sure that the field isn't empty
                if (value.Length == 0)
                {
                    throw new ArgumentException("No supervisor entered.");
                }
                supervisor = value;
            }
        }
    }
}
