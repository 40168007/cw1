﻿//Calum McCall
//This class contains the student class, its paramaters, two related methods and verification
//Last modified 9/10/2015

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Coursework_1
{
    class Student
    {
        private int matriculation; //Students matriculation number
        private string firstName; //Students first name
        private string secondName; //Students second name
        private string dateOfBirth; //Students date of birth
        private string course; //Course the student is on
        private int level = 1; //Level the student is at
        private int credits; //Credits student has
        private string award; //The students award

        public int Matriculation
        //Initialising and allowing access to matriculation
        {
            get
            {
                return matriculation;
            }
            set
            {
                //Ensures the matriculation number is between 40000 and 60000
                if ((value < 40000) || (value > 60000))
                {
                //Needs to be between 40000 and 60000
                throw new ArgumentException("Not between 40000 and 60000.");
                }
                matriculation = value;
            }
        }

        public string FirstName
        //Initialising and allowing access to users first name
        {
            get
            {
                return firstName;
            }
            set
            {
                //Makes sure the field isn't empty
                if (value.Length == 0)
                {
                    throw new ArgumentException("No first name entered.");
                }
                firstName = value;
            }
        }

        public string SecondName
        //Initialising and allowing access to users second name
        {
            get
            {
                return secondName;
            }
            set
            {
                //Makes sure the field isn't empty
                if (value.Length == 0)
                {
                    throw new ArgumentException("No second name entered.");
                }
                secondName = value;
            }
        }

        public string DateOfBirth
        //Initialising and allowing access to users date of birth
        {
            get
            {
                return dateOfBirth;
            }
            set
            {
                //Makes sure the field isn't empty
                if (value.Length == 0)
                {
                    throw new ArgumentException("No date of birth entered.");
                }
                dateOfBirth = value;
            }
        }

        public string Course
        //Initialising and allowing access to the users course
        {
            get
            {
                return course;
            }
            set
            {
                //Makes sure the field isn't empty
                if (value.Length == 0)
                {
                    throw new ArgumentException("No course entered.");
                }
                course = value;
            }
        }

        public int Level
        //Initialising and allowing access to level
        {
            get
            {
                return level;
            }
            set
            {
                //Level has to be between 1 and 4
                if (value < 1 || value > 4)
                {
                    throw new ArgumentException("Level must be between 1 and 4.");
                }
                level = value;
            }
        }

        public int Credits
        //Initialising and allowing access to the users credits
        {
            get
            {
                return credits;
            }
            set
            {
                //Credits has to be between 0 and 480
                if (value < 0 || value > 480)
                {
                    throw new ArgumentException("Credits must be between 0 and 480");
                }
                credits = value;
            }
        }

        public string Award
        //Initialising and allowing access to the users award
        {
            get
            {
                return award;
            }
            set
            {
                award = value;
            }
        }

        public int advance()
        {
            //Works out if the student should go up to the next level
            if (level == 4)
            {
                //If level 4 they cannot go any higher
                MessageBox.Show("Student is already year 4.");
            //Otherwise the need to have the required credits
            } else if (level == 1 && credits >= 120)
            {
                level = 2;
            } else if (level == 2 && credits >= 240)
            {
                level = 3;
            } else if (level == 3 && credits >= 360)
            {
                level = 4;
            } else
            {
                //They may not always have enough credits to advance
                MessageBox.Show("Not enough credits to advance.");
            }

            return level;
        }

        public string award_class(int credits)
        {
            //Works out the award the student should recieve
            if (credits < 360)
            {
                award = "Certificate of Higher Education";
            } else if (credits < 480)
            {
                award = "Degree";
            } else
            {
                award = "Honours Degree";
            }

            return award;
        }

    }
}
