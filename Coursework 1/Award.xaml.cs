﻿//Calum McCall
//This class displays the students award
//Last modified 9/10/2015

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coursework_1
{
    /// <summary>
    /// Interaction logic for Award.xaml
    /// </summary>
    public partial class Award : Window
    {
        public Award(int matriculation, string firstName, string secondName, string course, int level, string award)
        {
            InitializeComponent();

            //Sets the labels to the relevent information
            AwardMatricLabel.Content = (matriculation).ToString();
            AwardFirstNameLabel.Content = firstName;
            AwardSecondNameLabel.Content = secondName;
            AwardCourseLabel.Content = course;
            AwardLevelLabel.Content = level;
            AwardLabel.Content = award;

        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            //Closes windows
            this.Close();
        }
    }
}
