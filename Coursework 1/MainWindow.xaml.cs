﻿//Calum McCall
//This class displays the main window. It takes in information related to the student class.
//Last modified 22/10/2015

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Coursework_1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //Creates the student
        //if (!ResearchCheckBox.Checked)
        //{
        Student newStudent = new Student();
        //}
        //else
        //{
        //ResearchStudent newResearchStudent = new ResearchStudent();
        //}

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            //Clears all of the text boxes, information still held in class
            FirstNameInput.Text = String.Empty;
            SecondNameInput.Text = String.Empty;
            DobInput.Text = String.Empty;
            CourseInput.Text = String.Empty;
            MatricInput.Text = String.Empty;
            LevelInput.Text = String.Empty;
            CreditsInput.Text = String.Empty;
            TopicInput.Text = String.Empty;
            SupervisorInput.Text = String.Empty;
        }

        private void SetButton_Click(object sender, RoutedEventArgs e)
        {
            //Saves the information in the textboxes as the student
            //Validates the information to make sure that is meets the relevant requirements
            try
            {
                newStudent.FirstName = FirstNameInput.Text;
            }
            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
                MessageBoxResult error = MessageBox.Show("No first name entered.");
            }
            try
            {
                newStudent.SecondName = SecondNameInput.Text;
            }
            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
                MessageBoxResult error = MessageBox.Show("No second name entered.");
            }
            try
            {
                newStudent.DateOfBirth = DobInput.Text;
            }
            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
                MessageBoxResult error = MessageBox.Show("No date of birth entered.");
            }
            try
            {
                newStudent.Course = CreditsInput.Text;
            }
            catch (Exception excep)
            {
                Console.WriteLine(excep.Message);
                MessageBoxResult error = MessageBox.Show("No course entered.");
            }
            try
            {
                newStudent.Matriculation = Int32.Parse(MatricInput.Text);
            }
            catch (Exception excep)
            {
                Console.WriteLine(excep.ToString());
                MessageBoxResult error = MessageBox.Show("The matriculation number needs to be between 40000 and 60000.");
            }
            try
            {
                newStudent.Level = Int32.Parse(LevelInput.Text);
            }
            catch (Exception excep)
            {
                Console.WriteLine(excep.ToString());
                MessageBoxResult error = MessageBox.Show("Level must be between 1 and 4.");
            }
            try
            {
                newStudent.Credits = Int32.Parse(CreditsInput.Text);
            }
            catch (Exception excep)
            {
                Console.WriteLine(excep.ToString());
                MessageBoxResult error = MessageBox.Show("Credits must be between 0 and 480");
            }
        }

        private void GetButton_Click(object sender, RoutedEventArgs e)
        {
            //Retrieves the information on the student and displays it in the textboxes
            FirstNameInput.Text = newStudent.FirstName;
            SecondNameInput.Text = newStudent.SecondName;
            DobInput.Text = newStudent.DateOfBirth;
            CourseInput.Text = newStudent.Course;
            MatricInput.Text = (newStudent.Matriculation).ToString();
            LevelInput.Text = (newStudent.Level).ToString();
            CreditsInput.Text = (newStudent.Credits).ToString();
        }

        private void AwardButton_Click(object sender, RoutedEventArgs e)
        {
            //Displays the students award
            Award award = new Award(newStudent.Matriculation, newStudent.FirstName, newStudent.SecondName, newStudent.Course, newStudent.Level, newStudent.award_class(newStudent.Credits));
            award.Show();
        }

        private void AdvanceButton_Click(object sender, RoutedEventArgs e)
        {
            //Calls the advance method to check if the user is able to advance level
            newStudent.advance();

            LevelInput.Text = (newStudent.Level).ToString();
        }

        private void ResearchCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //If box is checked then the properties for the researcher are available
            TopicInput.IsEnabled = true;
            SupervisorInput.IsEnabled = true;

            ResearchStudent newResearchStudent = new ResearchStudent();

            CourseInput.Text = ("PhD");
        }
    }
}
